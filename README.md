# MLPR-武汉大学机器学习与模式识别课程

1. 大作业要求详见[`大作业`](https://gitee.com/whu_mmap_cliang/Course_MLPR/tree/master/大作业)

2. 课后作业及参考解答详见[`课后作业`](https://gitee.com/whu_mmap_cliang/Course_MLPR/tree/master/课后作业)

   *注：参考解答仅公布**一周**，请同学们及时查阅*

3. 实验指导及实验报告模板详见[`实验及报告`](https://gitee.com/whu_mmap_cliang/Course_MLPR/tree/master/实验及报告)

