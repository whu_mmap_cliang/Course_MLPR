## 在Modelarts上离线训练模型的步骤（以用cifar10训练resnet50为例）

### 1.下载数据和代码

 - 从官网上下载[cifar-10-binary.tar.gz](http://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz)，并解压

 - 从gitee上下载mindspore框架的[resnet](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/official/cv/resnet)，并解压

 - 在解压后的resnet文件夹中创建  **cifar-10-batches-bin** 和 **cifar-10-verify-bin** 的文件夹（**resnet.ipynb**文件是在第三步中创建的，这里先不用管）

   ![image-20210516232149626](https://i.loli.net/2021/05/16/IzuEeS7HqVQlF2i.png)

 - 将解压后的cifar-10数据集文件按下图所示放入上面提到的后两个文件夹中即可

   ![image-20210516215033068](https://i.loli.net/2021/05/16/VP6xlLSw7UE23Jo.png)

   

   ![image-20210516215124050](https://i.loli.net/2021/05/16/yFw21XjiYD8ConJ.png)
   
 - 由于代码版本问题需要对下载的代码（主要是主函数***train.py***）稍作修改

    1. 导入库部分做如下修改

      （1）THOR在训练中不会用到，因此将和它有关的语句屏蔽（不屏蔽会报import出错）

      （2）将储存在obs桶中的数据集进行映射到训练时的工作目录（obs桶目录要根据实际情况进行修改）

      ![image-20210517155942899](https://i.loli.net/2021/05/17/WSDjy1idh68e4ER.png)

    2. parser部分，主要是对默认值的修改

      ![image-20210517154355061](https://i.loli.net/2021/05/17/6WLHSuxMAnFrXUO.png)

    3. 在代码最后加上测试部分

      ![image-20210517161653645](https://i.loli.net/2021/05/17/u5qKezlYi2EFkHp.png)

      

   

### 2.上传代码

  - 登陆华为云控制台，左上角选择 **华北-北京四** 服务器

  - 在左边的服务列表中找到 **存储** 下的 **对象存储服务OBS** 并进入

    ![](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202105/14/144235tindgbkiklnbh5wv.png)

  - 选择右上角的图标创建桶

    ![image-20210516215332961](https://i.loli.net/2021/05/16/yRwM6pNvEOhzoaW.png)

  - 设定桶的名称，其他设置保持默认来创建一个桶

  - 单击进入刚创建的桶，选择左侧的对象，再点击上传对象，将resnet文件夹上传到obs中（建议先创建一个空文件夹，再把resnet上传到这个空文件夹里，这样方便以后做其他网络的训练）

    ![image-20210516220000078](https://i.loli.net/2021/05/16/fg8i7ErNlUHxqad.png)

    

### 3.训练模型

 - 登陆华为云控制台，确定左上角选择位于 **华北-北京四** 的服务器。

 - 在左边的任务列表中找到 **EI企业智能** 下的 **ModelArts** 服务并进入

   ![hwc1.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202105/14/144225vol1nj8excnzlbxx.png)

 - 选择左侧 **训练管理** 中选择 **训练作业**，单击**创建**

   ![image-20210517162318665](https://i.loli.net/2021/05/17/ZhMlRrzcsKDgVvO.png)

    - 创建中，**算法来源** 选择 **常用框架**。之后的**AI引擎**选择Ascend；**代码目录**选择上传到obs的resnet文件夹；**启动文件**选择修改好的train.py

      ![image-20210517162751436](https://i.loli.net/2021/05/17/KkbosNe2YEUC1QL.png)

    - **数据来源**也选择resnet文件夹。**训练输出位置**可以选一个空文件夹

      ![image-20210517162908574](https://i.loli.net/2021/05/17/M8EdwyrSB5gCka2.png)

    - 之后按步骤完成创建即可

 - 创建完成后会立即开始训练（训练时间较长，这里只训练10个epoch作为演示）

    - 点击训练作业名称进入详细界面后，可以通过**日志**预览训练过程

       - 训练过程截图

         ![image-20210517163522222](https://i.loli.net/2021/05/17/3KYc6OnJoewrq9G.png)

       - 测试截图

         ![image-20210517163555862](https://i.loli.net/2021/05/17/QOcMSxo1hByKIHN.png)



### 至此离线训练完成