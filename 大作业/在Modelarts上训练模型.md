## 在Modelarts上使用Jupyter Notebook开展模型训练的步骤（以用cifar10训练resnet50为例）

### 1.下载数据和代码

 - 从官网上下载[cifar-10-binary.tar.gz](http://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz)，并解压

 - 从gitee上下载mindspore框架的[resnet](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/official/cv/resnet)，并解压

 - 在解压后的resnet文件夹中创建  **cifar-10-batches-bin** 和 **cifar-10-verify-bin** 的文件夹（**resnet.ipynb**文件是在第三步中创建的，这里先不用管）

   ![image-20210516232149626](https://i.loli.net/2021/05/16/IzuEeS7HqVQlF2i.png)

 - 将解压后的cifar-10数据集文件按下图所示放入上面提到的后两个文件夹中即可

   ![image-20210516215033068](https://i.loli.net/2021/05/16/VP6xlLSw7UE23Jo.png)

   ![image-20210516215124050](https://i.loli.net/2021/05/16/yFw21XjiYD8ConJ.png)

   

### 2.上传代码

  - 登陆华为云控制台，左上角选择 **华北-北京四** 服务器

  - 在左边的服务列表中找到 **存储** 下的 **对象存储服务OBS** 并进入

    ![](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202105/14/144235tindgbkiklnbh5wv.png)

  - 选择右上角的图标创建桶

    ![image-20210516215332961](https://i.loli.net/2021/05/16/yRwM6pNvEOhzoaW.png)

  - 设定桶的名称，其他设置保持默认来创建一个桶

  - 单击进入刚创建的桶，选择左侧的对象，再点击上传对象，将resnet文件夹上传到obs中（建议先创建一个空文件夹，再把resnet上传到这个空文件夹里，这样方便以后做其他网络的训练）

    ![image-20210516220000078](https://i.loli.net/2021/05/16/fg8i7ErNlUHxqad.png)

    

### 3.创建notebook，并训练模型

 - 登陆华为云控制台，确定左上角选择位于 **华北-北京四** 的服务器。

 - 在左边的任务列表中找到 **EI企业智能** 下的 **ModelArts** 服务并进入

   ![hwc1.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202105/14/144225vol1nj8excnzlbxx.png)

 - 选择左侧 **开发环境** 中的 **Notebook**，再点击 **创建Notebook**

   ![image-20210516221901397](https://i.loli.net/2021/05/16/6lMqcQVAbpLNZEC.png)

    - 在工作环境中选择Ascend这一项

   ![image-20210516222122852](https://i.loli.net/2021/05/16/49MuAvfkKlXQpR2.png)

    - 在存储配置选择resnet文件夹的上一级目录（我的resnet文件夹位置为 **zyc1235/ch7-8/resnet**）

   ![image-20210516222440602](https://i.loli.net/2021/05/16/e6p1g24YvtzsQfC.png)

 - 创建好后打开notebook，将resnet文件夹同步

   ![image-20210516223512298](https://i.loli.net/2021/05/16/dp8Wx9tKhgCJ7r4.png)

 - 进到resnet文件夹下，创建一个Mindspore环境的notebook  ***(resnet.ipynb)***

   ![image-20210516222958427](https://i.loli.net/2021/05/16/svcnPwDu2ZXkYKz.png)

 - 打开**resnet.ipynb**，将resnet文件夹中自带的***train.py***里的内容分模块复制进来（由于版本问题，还需要对代码进行一些修改）

    1. 导入库部分，调整图中的两条语句，并修改部分模块的引用（加上***resnet.***）

       ![image-20210516230433617](https://i.loli.net/2021/05/16/GV1ta4dJCzA6BhS.png)

    2. parser部分，主要是修改一些默认值

       ![image-20210516230822261](https://i.loli.net/2021/05/16/P9KhYdbOMSH5zvx.png)

    3. 剩下的其他部分，主要是修改模块的引用（加上***resnet.***）

       ![image-20210516231345973](https://i.loli.net/2021/05/16/MwH8JRpFoeGxLrY.png)

 - 完成上述修改后，运行代码块即可开始训练，并会出现训练过程的预览

   ![image-20210516231824155](https://i.loli.net/2021/05/16/qlK2QVBUGx5ipIM.png)

 - 完成训练后可以使用下述代码块来查看训练模型的精度

   ![image-20210516231739374](https://i.loli.net/2021/05/16/x7GKmdlA45pu2Hv.png)