# MindSpore课堂实验

## 1 实验内容

- 花卉图像分类（flowers）
- 基于PCA和SVM的人脸识别（orl-faces)
- 基于多层感知机的鸢尾花图像分类（iris）
- 基于LeNet的鱼类图像分类（lenet）
- 基于U-Net的医学图像分割（unet）

## 2 实验说明

- 请大家提前将实验所需的文件上传至华为云OBS桶，使用操作参考[华为云平台介绍](https://gitee.com/whu_mmap_cliang/Course_MLPR/blob/master/大作业/华为云平台介绍.md)
- OBS桶中数据存储结构如下

```
'%s'%(桶名称)
├── flowers
│   ├── dataset
│	│	├── 5547758_eea9edfd54_n.jpg
│	│	├── 5547758_eea9edfd54_n.txt
│	│	├── ...
│	│	└── 23894449029_bf0f34d35d_n.txt
│   ├── model
│   └── output
├── iris
│   └── code.ipynb
├── lenet
|	├── four_fish
│	│	├── Hourse Mackerel
│	│	│	├── 00001.png
│	│	│	├── ...
│	│	│	└── 01000.png
│	│	├── Red Mullet
│	│	│	├── 00001.png
│	│	│	├── ...
│	│	│	└── 01000.png
│	│	├── Shrimp
│	│	│	├── 00001.png
│	│	│	├── ...
│	│	│	└── 01000.png
│	│	├── Trout
│	│	│	├── 00001.png
│	│	│	├── ...
│	│	│	└── 01000.png
│	│	├── fish_test_list.txt
│	│	└── fish_train_list.txt
│   ├── out_put
│   └── src
│ 		├── data_process.py
│ 		├── FishDatasetGenerator.py
│ 		└── main.py
├── orl-faces
│	├── code.ipynb
│   └── dataset.zip
└── unet
    ├── data
    │	├── test-volume.tif
    │	├── train-labels.tif
    │	└── train-volume.tif
    ├── output
    ├── src
    │	├── unet
	│	│	├── __init__.py
	│	│	├── unet_model.py
	│	│	└── unet_parts.py    
    │	├── config.py
    │	├── data_loader.py
    │	├── loss.py
    │	└── utils.py
    └── main.py
```

- 建议大家下载华为官方的[OBS Browser+](https://developer.huaweicloud.com/tools#section-2)以批量上传大文件和文件夹

<img src="https://gitee.com/messier42/picbed/raw/master/typora/huaweicloud-obs.png"/>

