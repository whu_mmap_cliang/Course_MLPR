# Importing libraries
import numpy as np
import cv2
import os
os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
# import argparse
from tqdm import tqdm
from keras.models import load_model
import preprocessing as pre
from get_pic_file_name import *

# parser = argparse.ArgumentParser()
# parser.add_argument('--shotname', type=str, default='shot1_436',
#                     help='which shot to test')
# parser.add_argument('--picname', type=str, default='1_436_0.jpg',
#                     help='which picture to test')
# args = parser.parse_args()


def read_images_from_directories(directories):
    for file in directories:
        image = cv2.imread(file)
        if image is not None:
            yield True, {'image': image, 'shot': file.split('/')[-2]}
        else:
            yield False, None
    while True:
        yield False, None

# print(args.picname)

# Model Directory
mod_dir = os.path.join(os.getcwd(), 'model')

# Importing models
model_info_path = os.path.join(mod_dir, 'model.xml')
model_path = os.path.join(mod_dir, 'model.h5')#'model.hdf5')
face_cascade = cv2.CascadeClassifier(model_info_path)
emotions = load_model(model_path)

# emotion_labels
emo = {0: 'angry', 1: 'disgust', 2: 'fear', 3: 'happy', 4: 'sad', 5: 'surprise', 6: 'neutral'}
squeeze_emo = {0: 'others', 1: 'crying', 2: 'shouting', 3: 'laughing'}

#font = cv2.FONT_HERSHEY_SIMPLEX
# ret, frame = cap.read()
# images = read_images_from_directories(file_list)
# print(images[0:10])
# frame = cv2.imread(pic_path)
for (_, frame) in enumerate(read_images_from_directories(file_list)):

    gray = cv2.cvtColor(frame[1]['image'], cv2.COLOR_BGR2GRAY)
    # detecting faces
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    # Getting boundaries for the faces
    vote = []
    for (x,y,w,h) in faces:
        # cv2.rectangle(frame, (x,y),(x+w,y+h),(255,0,0),3)
        roi_gray = gray[y:y+h, x:x+w]
        # roi_color = frame[y:y+h, x:x+w]
        roi_gray = cv2.resize(roi_gray, (48,48))
        face_emo = pre.preprocess_predict(roi_gray)
        emotion = emotions.predict(face_emo)
        # emotion_probability = np.max(emotion)
        emotion_label_arg = np.argmax(emotion)
        # print(emotion_label_arg)

        if emotion_label_arg == 0:
            squeeze_emotion_label_arg = 2
        elif emotion_label_arg == 1 or emotion_label_arg == 6:
            squeeze_emotion_label_arg = 0
        elif emotion_label_arg == 4 or emotion_label_arg == 2:
            squeeze_emotion_label_arg = 1
        else:
            squeeze_emotion_label_arg = 3
        # print(squeeze_emotion_label_arg)


        vote.append(squeeze_emotion_label_arg)
        # print(vote)

    if len(vote) == 0:
        print(0, frame[1]['shot'])
    else:
        print(max(vote, key=vote.count), frame[1]['shot'])


