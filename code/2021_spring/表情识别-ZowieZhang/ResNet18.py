#importing libraries
import os
os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
import keras
import keras_resnet.models



from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from keras.callbacks import ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
import preprocessing as pre

# Getting required directories
# Model Directory
mod_dir = os.path.join(os.getcwd(), 'model18')
# Data Directory
data_dir = os.path.join(os.getcwd(), 'data')
# Log Directory
log_dir = os.path.join(os.getcwd(), 'log')
# Base Directory
base_dir = os.getcwd()


# data loading and preprocessing
faces, emotions = pre.load_data(data_dir + "/fer2013/fer2013.csv")
faces = pre.preprocess_input(faces)
xtrain, xtest, ytrain, ytest = train_test_split(faces, emotions, test_size = 0.2, shuffle = True)





# parameters
batch_size = 64
num_epochs = 100
iut_shape = (48, 48, 1)
verbose = 1
num_classes = 7
patience = 30
l2_regularization=0.01

# data generator
data_generator = ImageDataGenerator(
                        featurewise_center = False,
                        featurewise_std_normalization = False,
                        rotation_range = 10,
                        width_shift_range = 0.1,
                        height_shift_range = 0.1,
                        zoom_range = .1,
                        horizontal_flip = True)

# Build model
shape, class_num = (48, 48, 1), 7
x = keras.layers.Input(shape)

model = keras_resnet.models.ResNet18(x, classes=class_num)
# model = keras_resnet.models.ResNet34(x, classes=class_num)
# model = keras_resnet.models.ResNet50(x, classes=class_num)

model.compile(optimizer = 'adam', 
        loss = 'categorical_crossentropy', 
        metrics = ['accuracy'])
model.summary()

# Defining callbacks
log_file_path = os.path.join(log_dir, 'training.log')
csv_logger = CSVLogger(log_file_path, append = False)
early_stop = EarlyStopping('val_loss', patience = patience)
reduce_lr = ReduceLROnPlateau('val_loss', factor = 0.1, patience = int(patience / 4), verbose = 1)
trained_models_path = mod_dir + '/facial_expression'
model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
model_checkpoint = ModelCheckpoint(model_names, 'val_loss', verbose = 1, save_best_only = True, period = 1)
callbacks = [model_checkpoint, csv_logger, early_stop, reduce_lr]

# Model Fitting
model.fit_generator(data_generator.flow(xtrain, ytrain, batch_size),
                        steps_per_epoch = len(xtrain) / batch_size,
                        epochs = num_epochs, verbose = 1, callbacks = callbacks,
                        validation_data = (xtest, ytest))

# Model Evaluation
train_score = model.evaluate(xtrain, ytrain, verbose = 0)
print(train_score)
test_score = model.evaluate(xtest, ytest, verbose = 0)
print(test_score)

# saving the final model
model.save(mod_dir + "/emotionrecognizer.h5")

