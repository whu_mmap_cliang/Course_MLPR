# My MPLR Homework

## Model

mini-Xception, ResNet18 and ResNet34

Mainly use the mini_Xception algorithm based on Exception and  utilize ResNet (50, 34 and 18) as baseline, which   the mini_Xception performs the best, all trained models in size have put into the model file.

Model.hdf5 is the used model in useModel.py, which is copy from _mini_xception-0.65.hdf5.

## Dataset

Fer2013

The Fer2013 dataset divides the expression into 7 classes, namely 0=angry, 1=disgust, 2=fear, 3=happy, 4=sad, 5=surprise, 6=neutral

You can download the dataset on Kaggle:https://www.kaggle.com/deadskull7/fer2013

## Train

```shell
# choose which model to use
python train.py
```



## Predict

```shell
# bulid a file list for prediction dataset such as expression
# use get_pic_file_name.py to load file name
# use useModel.py to get prediction result
python useModel.py 1> prediction_result.txt
```

