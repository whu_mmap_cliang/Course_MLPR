import os
import numpy as np 
import scipy.io as scio

dir_path = os.getcwd()
raw_data_path = os.path.join(dir_path, 'pic_file.txt')

file_list = []
with open(raw_data_path, 'r') as f:
	temp = f.readlines()
	for line in temp:		
		file_list.append(line[0:-1])

# print(file_list[0:10])