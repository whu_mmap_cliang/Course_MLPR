import scipy.io as sio
import numpy as np

with open('./unknown.txt', 'r') as f:
    null = f.read()
null = null.split()

null = np.array(list(map(lambda x: ('shot' + x).ljust(12), null)))
zeros = np.expand_dims(np.zeros(64), 0)

data = sio.loadmat('./converted_stage1.mat')
data['label'] = np.append(data['label'], zeros, 1)
data['shot'] = np.append(data['shot'], null, 0)

sio.savemat('./converted_stage2.mat', data)
