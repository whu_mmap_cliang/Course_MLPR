# label mappings
B = [0 1 2 3 4 5 6]
C = [2 1 0 3 1 2 0]
# convert
[~,idx] = ismember(label,B)
A = C(idx)
label = int64(A)
# store
clear A B C idx
save converted_stage1.mat label shot