#include "inc.h"

int main()
{
	// hardcoded...
	string input_list = "diff.txt";
	string base_path = "X:\\images\\";
	string output_index = "X:\\2nd.txt";

	// read all images
	vector<string> images;
	ReadData(input_list, images);

	// process them
	vector<string> success;
	unsigned cnt = 0, fail_cnt = 0, total = images.size();
	for (auto& i:images)
	{
		cnt++;
		cout << "[" << cnt << "/" << total << "]";

		auto path = process(i, base_path);
		if (path.empty())
		{
			fail_cnt++;
			cout << "Failed!" << endl;
			continue;
		}
		success.push_back(path);

		cout << " Done" << endl;
		cout << "\tSaved at: " << path << endl;
	}

	cout << fail_cnt << "/" << total << " failed!" << endl;

	// write list
	fstream index(output_index, std::fstream::out);
	if (!index.is_open())
	{
		std::cout << "failed to open " << output_index << std::endl;
		return -1;
	}

	for (auto& path:success)
	{
		auto name = fs::path(path).filename().replace_extension("").string();
		auto trim = name.substr(0, name.rfind('_'));
		index << trim << ',' << name << ',' << path << '\n';
	}

	index.close();
}