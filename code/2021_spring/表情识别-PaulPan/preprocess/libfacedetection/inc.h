#ifndef FACEPREPROCESS__INC_H_
#define FACEPREPROCESS__INC_H_

#include <map>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <regex>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define FACEDETECTION_EXPORT
#include <facedetectcnn.h>

using namespace std;
namespace fs = std::filesystem;
using namespace cv;

void ReadData(const string& file, vector<string>& filenames);
string process(const string& filename, const string& base_path);

#endif //FACEPREPROCESS__INC_H_
