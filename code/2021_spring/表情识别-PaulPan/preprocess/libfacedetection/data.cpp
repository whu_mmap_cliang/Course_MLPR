#include "inc.h"

void ReadData(const string& file, vector<string>& filenames)
{
	vector<string> paths;
	regex jpgFile("(.*)(.jpg)");

	// Extract all paths
	ifstream stream(file);
	string line;
	while (getline(stream, line)) paths.push_back(line);
	stream.close();

	// Get all images in those paths
	for (auto& f:paths)
		for (auto& p : fs::recursive_directory_iterator(f))
		{
			if (!p.is_regular_file())continue;
			auto filename = p.path().filename();
			if (!regex_match(p.path().filename().string(), jpgFile))continue;

			filenames.push_back(p.path().string());
		}
}