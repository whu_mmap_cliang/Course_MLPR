# 测试数据预处理

给定的表情测试数据集根据镜头进行分割，并且使用的是来自`BBC`电视剧的原始内容，未进行人脸识别及校正

我使用了`Dlib`和`Libfacedetection`对给定的数据进行预处理

## 一级预处理：`Dlib`68点校正

先对给定的$\approx9\text{GB}$的数据按镜头进行人脸识别以及通过68点标定进行校正

该部分核心代码见`dlib/dlib.cpp`

该模块主要有一个三个函数，具体作用如下：

```cpp
// 对外接口
// 该函数将对输入的图像任务进行切分，多线程处理数据
void align(
	const std::string& base_path,       // 校正后图像存放的目录基址
	const std::vector<image_t>& images, // 包含所有待校正图像的路径
	std::vector<std::string>& labels);  // 校正后图像的存放路径

// 每个线程的工作
// 该函数创建detector和landmarker并调用work函数处理每一张图片
void worker(
	int id, // 线程id，用于日志记录
	const std::string& base_path, // 校正后图像存放的目录基址
	const std::vector<image_t>& images, // 包含该线程待校正图像的路径
	std::vector<std::string>& labels); // 校正后图像的存放路径

// 人脸识别、校正、保存
std::string work(
	frontal_face_detector* detector, // worker中创建的detector
	shape_predictor* sp, // worker中创建的landmarker
	const std::string& base_path, // 校正后图像存放的目录基址
	const image_t& image); // 待校正的图像信息
```

但是在这一步骤中，`dlib`并不能处理所有的原始数据，一部分是因为对应镜头是噪声数据，一部分纯粹是`dlib`的识别器误差

我们可以通过简单的`shell`脚本找出未能被`dlib`处理的**镜头**（一个镜头内缺失部分图像不影响），然后将这部分数据交由`Libfacedetection`进行二次处理，二次处理仍未能处理的图像可以标记为`others`

在这一级处理中，原始镜头共`3422`个，识别并校正了`3105`个镜头

## 二级处理：`Libfacedetection`

在这一级处理中，只使用`libfacedetection`检测人脸并裁剪而不进行校正

在这一级中，剩下`3422-3105=317`个图像中，有`63`个无法识别，这`63`个镜头将直接被标记为`others`

核心代码在`libfacedetection/process.cpp`中。在处理过程中，先使用`libfacedetection`进行人脸识别，然后将结果按照人脸置信度降序排列，选择置信度最高的人脸作为该图像的待识别人脸；其中对识别出的人脸，在裁剪的时候多裁剪`20`像素作为`margin`

