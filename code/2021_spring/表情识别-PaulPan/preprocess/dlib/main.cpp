#include <filesystem>
#include "inc.h"

int main(int argc, char** argv)
{
	try
	{
		bool check = true;
		check &= (argc == 4);
		check &= exists(std::filesystem::path("assets/shape_predictor_68_face_landmarks.dat"));
		if (!check)
		{
			print_help();
			return 0;
		}

		auto target_dir = std::string(argv[1]);
		auto index_file = std::string(argv[2]);
		auto output_dir = std::string(argv[3]);

		std::cout << "[Walk] Begin to Walk" << std::endl;
		auto start = std::chrono::steady_clock::now();
		auto images = walk(target_dir);
		auto end = std::chrono::steady_clock::now();
		auto elapsed_seconds = std::chrono::duration<double>(end - start);
		std::cout << "[Walk] Done" << std::endl;
		std::cout << "[Walk] Count=" << images.size() << std::endl;
		std::cout << "[Walk] Elapsed Time=" << elapsed_seconds.count() << "s" << std::endl;

		std::vector<std::string> output_labels;
		std::cout << "[Align] Begin" << std::endl;
		start = std::chrono::steady_clock::now();
		align(output_dir, images, output_labels);
		end = std::chrono::steady_clock::now();
		elapsed_seconds = std::chrono::duration<double>(end - start);
		std::cout << "[Align] Done" << std::endl;
		std::cout << "[Align] Count=" << output_labels.size() << std::endl;
		std::cout << "[Align] Elapsed Time=" << elapsed_seconds.count() << "s" << std::endl;

		std::cout << "[Label] Begin" << std::endl;
		generate_label(index_file, output_labels);
		std::cout << "[Label] Done" << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << "\nexception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
	}
}
