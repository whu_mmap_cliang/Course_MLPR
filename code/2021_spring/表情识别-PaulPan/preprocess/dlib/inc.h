#ifndef FACEPREPROCESS__INC_H_
#define FACEPREPROCESS__INC_H_

#include <vector>
#include <iostream>

typedef std::pair<std::string, std::string> image_t;

void print_help();
std::vector<image_t> walk(const std::string& path);
void align(
	const std::string& base_path,
	const std::vector<image_t>& images,
	std::vector<std::string>& labels);
void generate_label(const std::string& path, const std::vector<std::string>& labels);

#endif //FACEPREPROCESS__INC_H_
