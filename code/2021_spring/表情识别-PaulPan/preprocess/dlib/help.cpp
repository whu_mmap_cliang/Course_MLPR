#include "inc.h"

void print_help()
{
	std::cout
		<< "./FacePreprocess"
		<< " <path to expressions>"
		<< " <file to store the index>"
		<< " <path to store the processed images>"
		<< std::endl;
	std::cout << "\t" << "<path to expressions>: "
			  << "The directory that contains shotX_Y/X_Y_i.jpg"
			  << std::endl;
	std::cout << "\t" << "<file to store the index>: "
			  << "The file that will store the index of the processed images"
			  << std::endl;
	std::cout << "\t" << "<path to store the processed images>: "
			  << "The directory that will store shotX_Y_i_aligned.jpg"
			  << std::endl;
	std::cout
		<< "shape_predictor_68_face_landmarks.dat should be stored at assets/shape_predictor_68_face_landmarks.dat"
		<< std::endl;
}