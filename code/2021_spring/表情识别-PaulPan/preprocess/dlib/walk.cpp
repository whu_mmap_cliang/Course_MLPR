#include <filesystem>
#include <regex>

#include "inc.h"

std::vector<image_t> walk(const std::string& path)
{
	std::vector<image_t> images;
	std::regex jpgFile("(.*)(.jpg)");

	for (auto& p : std::filesystem::recursive_directory_iterator(path))
	{
		if (!p.is_regular_file())continue;
		auto filename = p.path().filename();
		if (!std::regex_match(p.path().filename().string(), jpgFile))continue;

		images.emplace_back(
			p.path().string(),
			p.path().filename().replace_extension("").string());
	}

	return images;
}