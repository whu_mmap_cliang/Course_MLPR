#include <fstream>
#include <filesystem>
#include "inc.h"

void generate_label(const std::string& path, const std::vector<std::string>& labels)
{
	std::fstream file(path, std::fstream::out);
	if (!file.is_open())
	{
		std::cout << "failed to open " << path << std::endl;
		return;
	}

	for (auto& label:labels)
	{
		auto name = std::filesystem::path(label).filename().replace_extension("").string();
		auto trim = name.substr(0, name.rfind('_'));
		file << trim << ',' << name << ',' << label << '\n';
	}

	file.close();
}