from matplotlib import pyplot as plt
import numpy as np


def imshow(img, label):
    plt.title(label)
    img = img.numpy()
    img = np.transpose(img, (1, 2, 0))
    plt.imshow(img)
    plt.show()
