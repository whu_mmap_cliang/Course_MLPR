import torch
from torch import nn
from torchvision import models


class ResNet18ARM(nn.Module):
    def __init__(self, pretrained=True, num_classes=7, drop_rate=0):
        super(ResNet18ARM, self).__init__()
        self.drop_rate = drop_rate
        resnet = models.resnet18(pretrained)
        '''
            ResNet18:
            image -> (...) -> avgpool (512*1*1) -> FC
        '''
        self.features = nn.Sequential(*list(resnet.children())[:-2])
        self.arrangement = nn.PixelShuffle(16)
        self.arm = Amend()
        self.fc = nn.Linear(121, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = self.arrangement(x)
        x, alpha = self.arm(x)

        if self.drop_rate > 0:
            x = nn.Dropout(self.drop_rate)(x)

        x = x.view(x.size(0), -1)
        out = self.fc(x)

        return out, alpha


class Amend(nn.Module):
    def __init__(self, inplace=2):
        super(Amend, self).__init__()
        self.de_albino = nn.Conv2d(in_channels=1, out_channels=1,
                                   kernel_size=(32, 32), stride=(8, 8),
                                   padding=(0, 0), bias=False)
        self.bn = nn.BatchNorm2d(inplace)
        self.alpha = nn.Parameter(torch.tensor([1.0]))

    def forward(self, x):
        mask = torch.tensor([]).cuda()
        createVar = locals()
        for i in range(x.size(1)):
            createVar['x' + str(i)] = torch.unsqueeze(x[:, i], 1)
            createVar['x' + str(i)] = self.de_albino(createVar['x' + str(i)])
            mask = torch.cat((mask, createVar['x' + str(i)]), 1)
        x = self.bn(mask)
        xMax, _ = torch.max(x, 1, keepdim=True)
        global_mean = x.mean(dim=[0, 1])
        xMean = torch.mean(x, 1, keepdim=True)
        xMin, _ = torch.min(x, 1, keepdim=True)
        x = xMean + self.alpha * global_mean

        return x, self.alpha
