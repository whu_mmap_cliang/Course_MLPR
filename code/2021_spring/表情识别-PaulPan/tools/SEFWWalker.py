import os

trainIndex = open(r'E:\MLPR\SEFW\train.csv', 'w')
for filepath, _, files in os.walk(r'E:\MLPR\SEFW\Train\Train_Aligned_Faces'):
    for file in files:
        label = filepath.split('\\')[-1]
        trainIndex.write(label + ',./Train/Train_Aligned_Faces/' + label + '/' + file + '\n')
trainIndex.close()

valIndex = open(r'E:\MLPR\SEFW\val.csv', 'w')
for filepath, _, files in os.walk(r'E:\MLPR\SEFW\Val\Val_Aligned_Faces'):
    for file in files:
        label = filepath.split('\\')[-1]
        valIndex.write(label + ',./Val/Val_Aligned_Faces/' + label + '/' + file + '\n')
valIndex.close()
