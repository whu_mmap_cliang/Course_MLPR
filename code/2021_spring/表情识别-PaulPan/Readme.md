# My Awesome Facial Expression Recognizer

## Model

[ARM](https://github.com/JiaweiShiCV/Amend-Representation-Module) and [Residual Masking Network](https://github.com/phamquiluan/ResidualMaskingNetwork)

 [Network](./network/Network.py) is the original version of ARM while ([Network2](./network/Network2.py)) is a mixture of ARM and RMN

## Dataset

1. [RAF-DB](http://www.whdeng.cn/raf/model1.html)
2. SFEW

You have to generate an index for SEFW. Layout example:

```bash
➜  SFEW cat train.csv
Angry,./Train/Train_Aligned_Faces/Angry/Airheads_000519240_00000005.png
Angry,./Train/Train_Aligned_Faces/Angry/AlexEmma_000225840_00000024.png
Angry,./Train/Train_Aligned_Faces/Angry/AlexEmma_000846120_00000015.png
...
➜  SFEW cat val.csv
Angry,./Val/Val_Aligned_Faces/Angry/21_001108440_00000025.png
Angry,./Val/Val_Aligned_Faces/Angry/21_001108440_00000027.png
Angry,./Val/Val_Aligned_Faces/Angry/21_001108440_00000030.png
...
```

## Run

```bash
# RAF-DB base directory stored in $RAFDB
# SFEW base directory stored in $SFEW
# more options: python main.py -h

python main.py --rafdb_base=$RAFDB --sfew_base=$SFEW
```

## Predict

1. preprocess predict dataset (details in [README.md](./preprocess/README.md))
2. Run `label.py` (a index file is required : [example](./postprocess/combine.txt))
3. Run `postprocess/stage1.m`, which will do a mapping with labels (7 to 4)
4. Run `postprocess/stage2.py`, which will append null shots (The shots that could not be processed in preprocess stage)

## Result

Result based on `Network.py`

|   label    |  f1_score  |
| :--------: | :--------: |
|  `crying`  | 0.58999122 |
| `shouting` | 0.65480427 |
| `laughing` | 0.42377261 |

`Network2.py` requires a large amount of computation and Google's Colab could not afford it. (Since Huawei's ModelArts has an unacceptable experience, and I've made countless changes on my code, I don’t want to try it until they redesign the product.)

