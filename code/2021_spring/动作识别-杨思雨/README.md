## 第一步：准备动作数据集

### test数据集下载地址

链接：https://pan.baidu.com/s/1M0IW4fhvWPHNYFwDNo7DYg 

提取码：zav5 

大小：6.22G

### 标签定义

| label | meaning           | label | meaning           |
| :---- | :---------------- | ----- | ----------------- |
| 1     | holding_cloth     | 5     | open_door_enter   |
| 2     | eating            | 6     | carrying_bag      |
| 3     | go_up_down_stairs | 7     | smoking_cigarette |
| 4     | open_door_leave   | 0     | others            |

### train val 数据集下载

**where are the train/test datasets from**
1. holding_cloth
    - Charades

2. eating
    - Kinetics 400

3. go_up_down_stairs


4. open_door_leave
     - AVA (exit)

5. open_door_enter
    -  AVA (enter)

6. carrying_bag
    - Charades

7. smoking_cigarette
    - Kinetics 400

8. others

## 第二步 环境搭建

相关参考及教程：
https://pytorchvideo.org/docs/tutorial_classification

## 参考

https://github.com/xiaobai1217/Awesome-Video-Datasets/blob/main/README.md

@article{sigurdsson2016hollywood,
author = {Gunnar A. Sigurdsson and G{\"u}l Varol and Xiaolong Wang and Ivan Laptev and Ali Farhadi and Abhinav Gupta},
title = {Hollywood in Homes: Crowdsourcing Data Collection for Activity Understanding},
journal = {ArXiv e-prints},
eprint = {1604.01753}, 
year = {2016},
url = {http://arxiv.org/abs/1604.01753},
}

@inproceedings{TSN2016ECCV,
  author    = {Limin Wang and
               Yuanjun Xiong and
               Zhe Wang and
               Yu Qiao and
               Dahua Lin and
               Xiaoou Tang and
               Luc {Val Gool}},
  title     = {Temporal Segment Networks: Towards Good Practices for Deep Action Recognition},
  booktitle   = {ECCV},
  year      = {2016},
}

@inproceedings{inproceedings,
  author = {Carreira, J. and Zisserman, Andrew},
  year = {2017},
  month = {07},
  pages = {4724-4733},
  title = {Quo Vadis, Action Recognition? A New Model and the Kinetics Dataset},
  doi = {10.1109/CVPR.2017.502}
}

@inproceedings{gu2018ava,
  title={Ava: A video dataset of spatio-temporally localized atomic visual actions},
  author={Gu, Chunhui and Sun, Chen and Ross, David A and Vondrick, Carl and Pantofaru, Caroline and Li, Yeqing and Vijayanarasimhan, Sudheendra and Toderici, George and Ricco, Susanna and Sukthankar, Rahul and others},
  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages={6047--6056},
  year={2018}
}

Donahue, J., Hendricks, L. A., Guadarrama, S., Rohrbach, M., Venugopalan, S., Saenko, K., & Darrell, T. (2014). Long-term recurrent convolutional networks for visual recognition and description. arXiv preprint arXiv:1411.4389.Chicago