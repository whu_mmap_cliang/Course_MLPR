# IN_DATA_DIR="../../data/ava/videos"
# OUT_DATA_DIR="../../data/ava/videos_15min"

# if [[ ! -d "${OUT_DATA_DIR}" ]]; then
#   echo "${OUT_DATA_DIR} doesn't exist. Creating it.";
#   mkdir -p ${OUT_DATA_DIR}
# fi

# for video in $(ls -A1 -U ${IN_DATA_DIR}/*)
# do
#   out_name="${OUT_DATA_DIR}/${video##*/}"
#   if [ ! -f "${out_name}" ]; then
#     ffmpeg -ss 900 -t 901 -i "${video}" "${out_name}"
#   fi
# done

目录结构
|--ava
    |--enter-train
    |--enter-val
    |--exit-train
    |--exit-val
    |--short_videos
        |--enter-train
        |--enter-val
        |--exit-train
        |--exit-val
    |--frames
        |--enter-train
        |--enter-val
        |--exit-train
        |--exit-val


for dir in `cat dir.log`; do
    cd $dir;
    IN_DATA_DIR="./${dir}"
    OUT_DATA_DIR="../short_videos/${dir}"  

    if [[ ! -d "${OUT_DATA_DIR}" ]]; then
    echo "${OUT_DATA_DIR} doesn't exist. Creating it.";
    mkdir -p ${OUT_DATA_DIR}
    fi

    i = 2
    for line in `cat *.csv`  # csv
    do
        echo line $i:$line
        youtube_id=`echo $line | cut -d ',' -f1`
        video_type=`echo $line | cut -d ',' -f2`
        keyframe=`echo $line | cut -d ',' -f3`
        video_name = "${youtube_id}.${video_type}"
        # smoking eating carryingbag updownstairs others holding cloth
        start_time=`exp $keyframe -2`
        end_time=`exp $keyframe +2`
        # enter/exit 的特殊需求
        start_time=`exp $keyframe -2`
        end_time=`exp $keyframe +2`

        ffmpeg -ss "${start_time}" -t 4 -i "${video_name}" "${OUT_DATA_DIR}/${video_name}"

        let "i=$i+1"
    done

    cd ..
done

for dir in `cat dir.log`; 
do

IN_DATA_DIR="./short_videos"
OUT_DATA_DIR="./frames/${dir}"

if [[ ! -d "${OUT_DATA_DIR}" ]]; then
  echo "${OUT_DATA_DIR} doesn't exist. Creating it.";
  mkdir -p ${OUT_DATA_DIR}
fi

i = 2
for line in `cat $1`  # csv
do
    echo line $i:$line
    youtube_id=`echo $line | cut -d ',' -f1`
    video_type=`echo $line | cut -d ',' -f2`
    keyframe=`echo $line | cut -d ',' -f3`
    video_name = "${youtube_id}.${video_type}"
    # smoking eating carryingbag updownstairs others holding cloth
    start_time=`exp $keyframe -2`
    end_time=`exp $keyframe +2`
    # enter/exit 的特殊需求
    start_time=`exp $keyframe -2`
    end_time=`exp $keyframe +2`

    ffmpeg -ss "${start_time}" -t 4 -i "${IN_DATA_DIR}/${video_name}" -codec copy "${OUT_DATA_DIR}/${video_name}"

    let "i=$i+1"
done

echo "Finish cutting videos ! "
echo "Starting extracting frames ! "

if [[ ! -d "${OUT_DATA_DIR}" ]]; then
  echo "${OUT_DATA_DIR} doesn't exist. Creating it.";
  mkdir -p ${OUT_DATA_DIR}
fi

for video in $(ls -A1 -U ${IN_DATA_DIR}/*)
do
  video_name=${video##*/}

  if [[ $video_name = *".webm" ]]; then
    video_name=${video_name::-5}
  else
    video_name=${video_name::-4}
  fi

  out_video_dir=${OUT_DATA_DIR}/${video_name}/
  mkdir -p "${out_video_dir}"

  out_name="${out_video_dir}/${video_name}_%06d.jpg"

  ffmpeg -i "${video}" -r 30 -q:v 1 "${out_name}"
done


echo "done extracting ! "