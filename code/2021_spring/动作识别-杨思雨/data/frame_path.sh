#!/bin/bash

cd ./val/cloth
for clip in `ls`
do
	cd $clip
	
	# path num_frame label
	path="./val/cloth/${clip}"
	num_frame=`ls -l |grep "^-"|wc -l`
	label=1
	echo "${path} ${num_frame} ${label}" >> ../../val_cloth.txt
	
	cd ..
done
echo "val_cloth is done!!!"

