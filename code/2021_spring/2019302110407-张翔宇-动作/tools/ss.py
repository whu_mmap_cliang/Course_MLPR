# -*- coding: utf-8 -*-
import os
path = "./"
filelist = os.listdir(path) #该文件夹下所有的文件（包括文件夹）
count=1
for file in filelist:
    print(file)
for file in filelist:   #遍历所有文件
    Olddir=os.path.join(path,file)   #原来的文件路径
    if os.path.isdir(Olddir):   #如果是文件夹则跳过
        continue
    filename=os.path.splitext(file)[0]   #文件名
    filetype=os.path.splitext(file)[1]   #文件扩展名
    filename="zxw"
    Newdir=os.path.join(path,filename+str(count).zfill(4)+filetype)  #用字符串函数zfill 以0补全所需位数
    if(len(Olddir)>8):
        count1=count
        os.rename(Olddir,Olddir[:5]+'_'+str("{:0>6d}".format(count1))+'.jpg')#重命名
        count+=1