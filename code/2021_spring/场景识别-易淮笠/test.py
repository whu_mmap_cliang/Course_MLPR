import torch
from torch.autograd import Variable as V
import torchvision.models as models
import torchvision.transforms as transforms
import torch.nn.functional as F
import torchvision.datasets as datasets
import os
from PIL import Image
import random

import scipy.io as sio 


model_file = 'resnet18_places365.pth.tar'

model = models.__dict__['resnet18'](num_classes=365)
checkpoint = torch.load(model_file, map_location=lambda storage, loc: storage)
state_dict = {str.replace(k,'module.',''): v for k,v in checkpoint['state_dict'].items()}
model.load_state_dict(state_dict)
model.eval()

centre_crop = transforms.Compose([
        transforms.Resize((256,256)),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])


normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

file = "D:\\测试数据集\\scene.tar\\scene"

ans = dict()

valuemap = dict()

for i in range(1,365):
    valuemap[i] = 0

valuemap[274] = 10
valuemap[146] = 9
valuemap[222] = 9
valuemap[223] = 9
valuemap[321] = 9
valuemap[215] = 8
valuemap[208] = 6
valuemap[203] = 5
valuemap[285] = 5
valuemap[217] = 3
valuemap[75] = 2
valuemap[99] = 2



for root, dirs, files in os.walk(file):
    for d in dirs:   
        dist = os.path.join(file,d)
        for droot, ddirs, dfiles in os.walk(dist):
            dictionary = dict()
            for f in dfiles: 
                f = os.path.join(dist,f)
                img = Image.open(f)
                input_img = V(centre_crop(img).unsqueeze(0))
                logit = model.forward(input_img)
                h_x = F.softmax(logit, 1).data.squeeze()
                probs, idx = h_x.sort(0, True)
                key = valuemap[idx[1].item()]
                if(key==8):
                    temp = random.randint(1,10)
                    if(temp >=5):
                        key = 7
                if(key==5):
                    temp = random.randint(1,10)
                    if(temp >=5):
                        key = 4
                if(key==2):
                    temp = random.randint(1,10)
                    if(temp >=5):
                        key = 1

                if key in dictionary:
                    dictionary[key] += 1
                else :
                    dictionary[key] = 1
            print(d)
            ans[d]=max(dictionary,key=dictionary.get)


sio.savemat("ans.mat", ans)                